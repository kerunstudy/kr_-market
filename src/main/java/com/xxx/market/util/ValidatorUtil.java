package com.xxx.market.util;

import org.thymeleaf.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Kerun
 * @date 2021/12/22 - 23:51
 */
public class ValidatorUtil {

    private static final Pattern mobile_pattern = Pattern.compile("^1[3-9]\\d{9}$");

    /**
     * 判断手机号格式是否正确
     * @param mobile
     * @return
     */
    public static boolean isMobile(String mobile) {
        if (StringUtils.isEmpty(mobile)) {
            return false;
        }
        Matcher matcher = mobile_pattern.matcher(mobile);
        return matcher.matches();
    }
}
