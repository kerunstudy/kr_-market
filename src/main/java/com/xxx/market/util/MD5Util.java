package com.xxx.market.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

/**
 * @author Kerun
 * @date 2021/12/20 - 16:35
 */

@Component
public class MD5Util {

    public static String md5(String src) {
        return DigestUtils.md5Hex(src);
    }

    public static final String SALT = "1a2b3c4d";

    public static String inputPassToFromPass(String inputPass) {
        String str = "" + SALT.charAt(0)+SALT.charAt(2)+inputPass+SALT.charAt(5)+SALT.charAt(4);
        return md5(str);
    }

    public static String formPassToDBPass(String fromPass, String salt) {
        String str = "" + salt.charAt(0) + salt.charAt(2) + fromPass + salt.charAt(5) + salt.charAt(4);
        return md5(str);
    }

    public static String inputPassToDBPass(String inputPass, String salt) {
        String fromPass = inputPassToFromPass(inputPass);
        String dbPass = formPassToDBPass(fromPass, salt);
        return dbPass;
    }

    public static void main(String[] args) {
        // d3b1294a61a07da9b49b6e22b2cbd7f9
//        System.out.println(inputPassToFromPass("123456"));
        // b7797cce01b4b131b433b6acf4add449
//        System.out.println(formPassToDBPass("d3b1294a61a07da9b49b6e22b2cbd7f9", SALT));
        // b7797cce01b4b131b433b6acf4add449
        System.out.println(inputPassToDBPass("111111", SALT));
    }
}
