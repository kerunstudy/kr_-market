package com.xxx.market.validator;

import com.xxx.market.util.ValidatorUtil;
import com.xxx.market.validator.annotation.IsMobile;
import org.thymeleaf.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Kerun
 * @date 2022/4/29 - 17:59
 */
public class IsMobileValidator implements ConstraintValidator<IsMobile,String> {

    private boolean required = false;

    @Override
    public void initialize(IsMobile constraintAnnotation) {
        required = constraintAnnotation.required();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (!required) {
            // 手机号非必填并且为空
            if (StringUtils.isEmpty(value)) {
                return true;
            }
        }

        return ValidatorUtil.isMobile(value);
    }
}
