package com.xxx.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxx.market.domain.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
public interface UserMapper extends BaseMapper<User> {

}
