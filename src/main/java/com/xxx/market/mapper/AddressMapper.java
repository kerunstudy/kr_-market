package com.xxx.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxx.market.domain.Address;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kerun
 * @since 2022-05-03
 */
public interface AddressMapper extends BaseMapper<Address> {

}
