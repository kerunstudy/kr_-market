package com.xxx.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxx.market.domain.Order;
import com.xxx.market.vo.OrderVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
public interface OrderMapper extends BaseMapper<Order> {

    List<OrderVo> getOrderVoList(@Param("userId") Long userId);
}
