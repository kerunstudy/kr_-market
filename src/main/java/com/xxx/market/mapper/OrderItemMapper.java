package com.xxx.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxx.market.domain.OrderItem;
import com.xxx.market.vo.OrderItemVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
public interface OrderItemMapper extends BaseMapper<OrderItem> {

    List<OrderItemVo> getOrderItemVoList(@Param("orderId") Long orderId);
}
