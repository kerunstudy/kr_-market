package com.xxx.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxx.market.domain.Favor;
import com.xxx.market.vo.FavorVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
public interface FavorMapper extends BaseMapper<Favor> {

    List<FavorVo> getFavorVoList(@Param("userId") Long userId);
}
