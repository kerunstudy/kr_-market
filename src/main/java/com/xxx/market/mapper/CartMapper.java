package com.xxx.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxx.market.domain.Cart;
import com.xxx.market.vo.CartVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kerun
 * @since 2022-05-02
 */
public interface CartMapper extends BaseMapper<Cart> {

    CartVo getCartVo(@Param("userId") Long userId,@Param("goodsId") Long goodsId);

    List<CartVo> getCartVoList(@Param("userId")Long userId);

    void clear(@Param("userId") Long userId);
}
