package com.xxx.market.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxx.market.domain.Favor;
import com.xxx.market.vo.FavorVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
public interface IFavorService extends IService<Favor> {

    List<FavorVo> getFavorVoList(Long userId);
}
