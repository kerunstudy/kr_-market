package com.xxx.market.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxx.market.domain.OrderItem;
import com.xxx.market.vo.CartVo;
import com.xxx.market.vo.OrderItemVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
public interface IOrderItemService extends IService<OrderItem> {

    void bindToOrder(List<CartVo> cartVoList, Long orderId);

    List<OrderItemVo> getOrderItemVoList(Long orderId);
}
