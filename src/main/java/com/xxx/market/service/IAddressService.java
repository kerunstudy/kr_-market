package com.xxx.market.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxx.market.domain.Address;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kerun
 * @since 2022-05-03
 */
public interface IAddressService extends IService<Address> {

    List<Address> getList(Long userId);
}
