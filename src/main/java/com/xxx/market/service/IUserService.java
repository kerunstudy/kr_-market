package com.xxx.market.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxx.market.domain.User;
import com.xxx.market.vo.LoginVo;
import com.xxx.market.vo.RespBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
public interface IUserService extends IService<User> {

    RespBean doLogin(LoginVo loginVo);
}
