package com.xxx.market.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxx.market.domain.Cart;
import com.xxx.market.mapper.CartMapper;
import com.xxx.market.service.ICartService;
import com.xxx.market.vo.CartVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kerun
 * @since 2022-05-02
 */
@Service
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements ICartService {

    @Autowired
    CartMapper cartMapper;

    @Override
    public CartVo getCartVo(Long userId, Long goodsId) {
        return cartMapper.getCartVo(userId, goodsId);
    }

    @Override
    public List<CartVo> getCartVoList(Long userId) {
        return cartMapper.getCartVoList(userId);
    }

    @Override
    public void clear(Long userId) {
        cartMapper.clear(userId);
    }
}
