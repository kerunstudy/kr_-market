package com.xxx.market.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxx.market.domain.OrderItem;
import com.xxx.market.mapper.OrderItemMapper;
import com.xxx.market.service.IOrderItemService;
import com.xxx.market.vo.CartVo;
import com.xxx.market.vo.OrderItemVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements IOrderItemService {

    @Autowired
    OrderItemMapper orderItemMapper;

    @Override
    public void bindToOrder(List<CartVo> cartVoList, Long orderId) {
        for (CartVo cartVo : cartVoList) {
            OrderItem orderItem = new OrderItem();
            orderItem.setOrderId(orderId);
            orderItem.setGoodsId(cartVo.getGoodsId());
            orderItem.setGoodsCount(cartVo.getGoodsCount());
            orderItemMapper.insert(orderItem);
        }
    }

    @Override
    public List<OrderItemVo> getOrderItemVoList(Long orderId) {
        return orderItemMapper.getOrderItemVoList(orderId);
    }
}
