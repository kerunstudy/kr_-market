package com.xxx.market.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxx.market.domain.User;
import com.xxx.market.exception.GlobalException;
import com.xxx.market.mapper.UserMapper;
import com.xxx.market.service.IUserService;
import com.xxx.market.util.MD5Util;
import com.xxx.market.vo.LoginVo;
import com.xxx.market.vo.RespBean;
import com.xxx.market.vo.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public RespBean doLogin(LoginVo loginVo) {
        String mobile = loginVo.getMobile();
        String password = loginVo.getPassword();

        // 根据手机号从数据库取出 User 对象
        User user = userMapper.selectById(mobile);
        // 手机号不存在于数据库中
        if (user == null) {
            throw new GlobalException(RespBeanEnum.LOGIN_ERROR);
        }
        // 密码错误
        if (!MD5Util.formPassToDBPass(password, user.getSalt()).equals(user.getPassword())) {
            System.out.println("转换后密码：" + MD5Util.formPassToDBPass(password, user.getSalt()));
            System.out.println("数据库密码: " + user.getPassword());
            throw new GlobalException(RespBeanEnum.LOGIN_ERROR);
        }
        // 更新用户最新一次登录时间
        user.setLastLoginTime(new Date());
        userMapper.updateById(user);

        // 账号密码都正确，登录成功
        return RespBean.success(user);
    }
}
