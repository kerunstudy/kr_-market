package com.xxx.market.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxx.market.domain.Favor;
import com.xxx.market.mapper.FavorMapper;
import com.xxx.market.service.IFavorService;
import com.xxx.market.vo.FavorVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
@Service
public class FavorServiceImpl extends ServiceImpl<FavorMapper, Favor> implements IFavorService {

    @Autowired
    FavorMapper favorMapper;

    @Override
    public List<FavorVo> getFavorVoList(Long userId) {
        return favorMapper.getFavorVoList(userId);
    }
}
