package com.xxx.market.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxx.market.domain.Order;
import com.xxx.market.mapper.OrderMapper;
import com.xxx.market.service.IOrderService;
import com.xxx.market.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Autowired
    OrderMapper orderMapper;

    @Override
    public List<OrderVo> getOrderVoList(Long userId) {
        return orderMapper.getOrderVoList(userId);
    }
}
