package com.xxx.market.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxx.market.domain.Address;
import com.xxx.market.domain.Cart;
import com.xxx.market.mapper.AddressMapper;
import com.xxx.market.service.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kerun
 * @since 2022-05-03
 */
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, Address> implements IAddressService {

    @Autowired
    AddressMapper addressMapper;

    @Override
    public List<Address> getList(Long userId) {
        return addressMapper.selectList(new QueryWrapper<Address>().eq("user_id", userId));
    }
}
