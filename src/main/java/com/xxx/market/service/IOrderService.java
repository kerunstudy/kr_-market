package com.xxx.market.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxx.market.domain.Order;
import com.xxx.market.vo.OrderVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
public interface IOrderService extends IService<Order> {

    List<OrderVo> getOrderVoList(Long userId);
}
