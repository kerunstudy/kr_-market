package com.xxx.market.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxx.market.domain.Cart;
import com.xxx.market.vo.CartVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kerun
 * @since 2022-05-02
 */
public interface ICartService extends IService<Cart> {

    CartVo getCartVo(Long userId, Long goodsId);

    List<CartVo> getCartVoList(Long userId);

    void clear(Long userId);
}
