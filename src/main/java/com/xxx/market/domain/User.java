package com.xxx.market.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("kr_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID,手机号码
     */
    private Long id;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * MD5+salt
     */
    private String password;

    private String salt;

    /**
     * 头像图片所在位置
     */
    private String avatars;

    /**
     * M:男;F:女
     */
    private String gender;

    /**
     * 会员级别, 0普通会员,1VIP,2超级VIP
     */
    private Integer userLevel;

    /**
     * 注册时间
     */
    private Date registerTime;

    /**
     * 最后一次登录时间
     */
    private Date lastLoginTime;


}
