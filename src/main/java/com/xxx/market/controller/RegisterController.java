package com.xxx.market.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xxx.market.domain.User;
import com.xxx.market.exception.GlobalException;
import com.xxx.market.service.IUserService;
import com.xxx.market.util.MD5Util;
import com.xxx.market.vo.RegisterVo;
import com.xxx.market.vo.RespBean;
import com.xxx.market.vo.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Date;

/**
 * @author Kerun
 * @date 2022/5/6 - 17:21
 */

@Controller
public class RegisterController {

    @Autowired
    IUserService userService;

    @PostMapping(value = "/doRegister")
    @ResponseBody
    public RespBean doRegister(@Valid RegisterVo registerVo, HttpSession session) {
        // 1. 手机号已存在
        Long mobile = Long.parseLong(registerVo.getMobile());
        User one = userService.getOne(new QueryWrapper<User>().eq("id", mobile));
        if (one != null) {
            throw new GlobalException(RespBeanEnum.MOBILE_EXIST);
        }
        // 2. 两次输入的密码不相同
        if (!registerVo.getFirstPass().equals(registerVo.getConfirmPass())) {
            throw new GlobalException(RespBeanEnum.PASSWORD_DIFFERENT);
        }
        // 3. 手机号不存在且密码相同，生成新用户插入数据库
        User registerOne = new User();
        registerOne.setId(mobile);
        registerOne.setNickname("商城用户_1111");
        registerOne.setPassword(MD5Util.formPassToDBPass(registerVo.getFirstPass(), "1a2b3c4d"));
        registerOne.setSalt("1a2b3c4d");
        registerOne.setAvatars("/assets/images/chaiquan1.png");
        registerOne.setGender("M");
        registerOne.setUserLevel(0);
        registerOne.setRegisterTime(new Date());
        registerOne.setLastLoginTime(new Date());
        userService.save(registerOne);

        return RespBean.success();
    }
}
