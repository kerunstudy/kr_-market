package com.xxx.market.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xxx.market.domain.User;
import com.xxx.market.exception.GlobalException;
import com.xxx.market.service.IUserService;
import com.xxx.market.util.MD5Util;
import com.xxx.market.vo.EditVo;
import com.xxx.market.vo.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userService;

    @GetMapping("/info")
    public String toUserInfo() {
        return "userInfo";
    }

    /**
     * 修改用户个人信息
     * @param editVo
     * @param session
     * @return
     */
    @PostMapping("/editInfo")
    public String editInfo(@Valid EditVo editVo, HttpSession session) {
        // 1. 获取从前的User对象和userId
        User user = (User) session.getAttribute("user");
        Long oldId = user.getId();
        // 2. 更新user对象数据
        // 数据库已存在手机号，更新失败
        User one = userService.getOne(new QueryWrapper<User>().eq("id", editVo.getPhone()));
        // nickName和gender只要其中一个改过就根据oldId更新数据库
        if (!user.getNickname().equals(editVo.getNickname()) || !user.getGender().equals(editVo.getGender())) {
            user.setNickname(editVo.getNickname());
            user.setGender(editVo.getGender());
        }else {
            // nickName和gender都没有改过，并且手机号存在，报错
            if (one != null) {
                throw new GlobalException(RespBeanEnum.MOBILE_EXIST);
            }else {
                // 手机号不存在就根据oldId更改手机号
                user.setId(Long.parseLong(editVo.getPhone()));
            }
        }

//        user.setNickname(editVo.getNickname());
//        user.setGender(editVo.getGender());
//        System.out.println(editVo.getPhone());
//        System.out.println(editVo.getNickname());
//        System.out.println(editVo.getGender());
        // 3. 更新session域中的user对象
        session.setAttribute("user", user);
        // 4. 更新user到数据库
        userService.update(user, new QueryWrapper<User>().eq("id", oldId));

        return "userInfo";
    }

    @Transactional
    @PostMapping("/editPass")
    public String editPass(String oldPass,String newPass, String confirmPass, HttpSession session) {
        String encryptOld = MD5Util.inputPassToDBPass(oldPass, MD5Util.SALT);
        User user = (User) session.getAttribute("user");
        // 原密码输入错误
        if (!user.getPassword().equals(encryptOld)) {
            throw new GlobalException(RespBeanEnum.PASSWORD_ERROR);
        }
        // 新密码与确认密码不相同
        if (!newPass.equals(confirmPass)) {
            throw new GlobalException(RespBeanEnum.PASSWORD_DIFFERENT);
        }
        // 通过验证后更新user对象
        user.setPassword(MD5Util.inputPassToDBPass(newPass, MD5Util.SALT));
        session.setAttribute("user", user);
        // 更新user对象到数据库
        userService.updateById(user);

        return "userInfo";
    }
}
