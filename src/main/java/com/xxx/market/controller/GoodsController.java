package com.xxx.market.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xxx.market.domain.Goods;
import com.xxx.market.service.IGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
@Controller
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    IGoodsService goodsService;

    @RequestMapping("/list")
    public String toGoodsList(HttpSession session) {
        List<Goods> goodsList = goodsService.getList();
        session.setAttribute("goodsList", goodsList);

        return "products";
    }

    /**
     * 跳转商品详情页
     * @param goodsId
     * @param session
     * @return
     */
    @RequestMapping("/info/{goodsId}")
    public String toGoodsInfo(@PathVariable("goodsId") Long goodsId, HttpSession session) {
        Goods goods = goodsService.getOne(new QueryWrapper<Goods>().eq("id", goodsId));
        session.setAttribute("goods", goods);

        return "goodsInfo";
    }

    @RequestMapping("/info")
    public String toGoodsInfo() {
        return "goodsInfo";
    }

}
