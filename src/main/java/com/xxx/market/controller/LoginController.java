package com.xxx.market.controller;

import com.xxx.market.domain.User;
import com.xxx.market.service.IUserService;
import com.xxx.market.util.CookieUtil;
import com.xxx.market.util.UUIDUtil;
import com.xxx.market.vo.LoginVo;
import com.xxx.market.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author Kerun
 * @date 2022/5/1 - 14:30
 */
@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    IUserService userService;

    @Autowired
    RedisTemplate redisTemplate;

    @ResponseBody
    @PostMapping("/doLogin")
    public RespBean doLogin(@Valid LoginVo loginVo, HttpSession httpSession, HttpServletRequest request, HttpServletResponse response) {
        RespBean r = userService.doLogin(loginVo);
        // 若登录成功，则将user对象存储到请求域中
        if (r.getCode() == 200) {
//            将user存入Redis，实现多台服务器的缓存共享，从而实现用户免登陆功能
//            // 生成Cookie
//            String ticket = UUIDUtil.uuid();
//            // 将用户信息存入redis中
//            redisTemplate.opsForValue().set("user:" + ticket, (User) r.getObj());
//            // request.getSession().setAttribute(ticket, user);
//            CookieUtil.setCookie(request, response, "userTicket", ticket);
            httpSession.setAttribute("user",(User) r.getObj());
        }

        return r;
    }

}
