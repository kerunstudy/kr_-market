package com.xxx.market.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xxx.market.domain.Address;
import com.xxx.market.domain.Cart;
import com.xxx.market.domain.Order;
import com.xxx.market.domain.User;
import com.xxx.market.service.*;
import com.xxx.market.vo.CartVo;
import com.xxx.market.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kerun
 * @since 2022-05-02
 */
@Controller
@RequestMapping("/cart")
public class CartController {

    @Autowired
    IAddressService addressService;

    @Autowired
    ICartService cartService;

    @Autowired
    IGoodsService goodsService;

    @Autowired
    IOrderService orderService;

    @Autowired
    IOrderItemService orderItemService;

    @Transactional
    @PostMapping("/add/{goodsId}")
    @ResponseBody
    public RespBean add(@PathVariable("goodsId") Long goodsId, HttpSession session) {
        Long userId = ((User)session.getAttribute("user")).getId();
        // equals
        Cart one = cartService.getOne(new QueryWrapper<Cart>().eq("user_id", userId).eq("goods_id", goodsId));
        // 第一次将商品加入购物车
        if (one == null) {
            Cart cart = new Cart();
            cart.setUserId(userId);
            cart.setGoodsId(goodsId);
            cart.setGoodsCount(1);
            cart.setCreateTime(new Date());
            cartService.save(cart);
        }else {
            // 商品已存在于购物车内
            if (one.getIsDelete() == 0) {
                one.setGoodsCount(one.getGoodsCount() + 1);
                cartService.updateById(one);
            }else { // 商品之前加入过购物车，但是被删除了，现在重新加入
                one.setIsDelete(0);
                one.setGoodsCount(1);
                cartService.updateById(one);
            }
        }

        return RespBean.success();
    }


    /**
     * 跳转购物车页面
     * @param session
     * @return
     */
    @RequestMapping("/list")
    public String toCart(HttpSession session) {
        Long userId = ((User)session.getAttribute("user")).getId();
        List<Address> addressList = addressService.getList(userId);
        List<CartVo> cartVoList = cartService.getCartVoList(userId);
        session.setAttribute("cartVoList", cartVoList);
        session.setAttribute("addressList", addressList);

        return "cart";
    }

    @PutMapping("/addOne/{goodsId}")
    @ResponseBody
    public RespBean addOne(@PathVariable("goodsId") Long goodsId, HttpSession session) {
        Long userId = ((User)session.getAttribute("user")).getId();
        Cart one = cartService.getOne(new QueryWrapper<Cart>().eq("user_id", userId).eq("goods_id", goodsId));
        one.setGoodsCount(one.getGoodsCount() + 1);
        cartService.updateById(one);

        return RespBean.success();
    }

    @PutMapping("/subOne/{goodsId}")
    @ResponseBody
    public RespBean subOne(@PathVariable("goodsId") Long goodsId, HttpSession session) {
        Long userId = ((User)session.getAttribute("user")).getId();
        Cart one = cartService.getOne(new QueryWrapper<Cart>().eq("user_id", userId).eq("goods_id", goodsId));
        one.setGoodsCount(one.getGoodsCount() - 1);
        cartService.updateById(one);

        return RespBean.success();
    }

    @DeleteMapping("{goodsId}")
    @ResponseBody
    public RespBean del(@PathVariable Long goodsId, HttpSession session) throws InterruptedException {
        Long userId = ((User)session.getAttribute("user")).getId();
        Cart one = cartService.getOne(new QueryWrapper<Cart>().eq("user_id", userId).eq("goods_id", goodsId));
        one.setIsDelete(1);
        cartService.updateById(one);
        // 给0.3秒让数据库进行更新操作
        Thread.sleep(300);

        return RespBean.success();
    }

    // 生成订单
    @PostMapping("/doOrder/{addressId}")
    @ResponseBody
    public RespBean doOrder(@PathVariable("addressId") Long addressId, HttpSession session) {
        Long userId = ((User)session.getAttribute("user")).getId();
        // 1. 生成订单
        Order order = new Order();
        order.setUserId(userId);
        order.setDeliveryAddrId(addressId);
        // 购物端 1pc,2android,3ios
        order.setOrderChannel(1);
        // 订单状态,0新建未支付,1已支付,2已发货,3已收货,4已退款,5已完成
        order.setStatus(0);
        order.setCreateTime(new Date());
        order.setIsCancel(0);
        orderService.save(order);
        List<CartVo> cartVoList = cartService.getCartVoList(userId);
        orderItemService.bindToOrder(cartVoList, order.getId());
        // 2. 清空购物车
        cartService.clear(userId);

        // 3. 跳转到订单详情页面
        return RespBean.success();
    }
}
