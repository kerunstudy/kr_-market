package com.xxx.market.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xxx.market.domain.Address;
import com.xxx.market.domain.User;
import com.xxx.market.service.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kerun
 * @since 2022-05-03
 */
@Controller
@RequestMapping("/address")
public class AddressController {

    @Autowired
    IAddressService addressService;

    @RequestMapping("/list")
    public String toAddressList(HttpSession session) {
        Long userId = ((User) session.getAttribute("user")).getId();
        // 1. 根据userId查询地址
        List<Address> addressList = addressService.getList(userId);
        // 2. 将addressList放入session
        session.setAttribute("addressList", addressList);

        return "addressList";
    }

    /**
     * 编辑按钮
     * @param addressId
     * @param session
     * @return
     */
    @RequestMapping("/info/{addressId}")
    public String toAddressInfo(@PathVariable(value = "addressId") Long addressId, HttpSession session) {
        if (addressId != null) {
            // 1. 根据addressId查询地址
            Address one = addressService.getOne(new QueryWrapper<Address>().eq("id", addressId));
            // 2. 放入session
            session.setAttribute("addressInfo", one);
        }

        return "editAddress";
    }

    /**
     * 新增或编辑地址
     * @param address
     * @param session
     * @return
     */
    @PostMapping("/editAddress")
    public String editAddress(Address address, HttpSession session) {
        Long userId = ((User) session.getAttribute("user")).getId();
        // 除userId外，其他属性都会从前端传过来
        address.setUserId(userId);
        // 若取得到id，则说明是编辑按钮跳过来的，则更新数据库信息
        if (address.getId() != null) {
            // 若要将此地址设置为默认地址，需先将之前的默认地址取消
            if (address.getIsDefault() == 1) {
                Address one = addressService.getOne(new QueryWrapper<Address>().eq("is_default", 1).eq("user_id", userId));
                one.setIsDefault(0);
                addressService.updateById(one);
            }
            addressService.updateById(address);
        }else {
            // 若取不到id，则说明是新增按钮跳过来的，则插入新的地址
            addressService.save(address);
            System.out.println(address);
        }

        return "redirect:/address/list";
    }

    @Transactional
    @RequestMapping("/del/{addressId}")
    public String del(@PathVariable("addressId") Long addressId) {
        addressService.removeById(addressId);

        return "redirect:/address/list";
    }
}
