package com.xxx.market.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
@Controller
@RequestMapping("/order-item")
public class OrderItemController {

}
