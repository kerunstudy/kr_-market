package com.xxx.market.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * 页面跳转Controller
 * @author Kerun
 * @date 2022/4/27 - 16:23
 */

@Controller
public class PageJumpController {

    /**
     * 跳转关于页面
     * @return
     */
    @RequestMapping("/about")
    public String toAbout() {
        return "about";
    }

    /**
     * 跳转联系我们页面
     * @return
     */
    @RequestMapping("/contact")
    public String toContact() {
        return "contact";
    }

    /**
     * 跳转个人中心页面
     * @return
     */
    @RequestMapping("/userInfo")
    public String toUserInfo() {
        return "userInfo";
    }

    /**
     * 跳转登录页面
     * @return
     */
    @RequestMapping("/toLogin") // /toLogin = localhost:8080/项目名/toLogin
    public String toLogin() {
        return "login";
    }

    /**
     * 跳转注册页面
     * @return
     */
    @RequestMapping("/register")
    public String toRegister() {
        return "register";
    }

    /**
     * 跳转编辑个人信息页面
     * @return
     */
    @RequestMapping("/editUserInfo")
    public String toEditUserInfo() {
        return "editUserInfo";
    }

    /**
     * 跳转修改密码页面
     * @return
     */
    @RequestMapping("/editUserPass")
    public String toEditUserPass() {
        return "editPass";
    }

    /**
     * 跳转修改地址页面
     * @return
     */
    @RequestMapping("/toEditAddress")
    public String toEditAddress(HttpSession session) {
        // 新增address和编辑address共用一个html页面：editAddress，如果点新增按钮之前点过编辑按钮，页面就会有数据展示
        // 所以跳转修改地址页面之前，如果已经有addressInfo对象存在，则需把它置空
        if (!(null == session.getAttribute("addressInfo"))) {
            session.setAttribute("addressInfo", null);
        }

        return "editAddress";
    }
}
