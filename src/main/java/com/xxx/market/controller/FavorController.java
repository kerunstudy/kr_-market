package com.xxx.market.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xxx.market.domain.Favor;
import com.xxx.market.domain.User;
import com.xxx.market.service.IFavorService;
import com.xxx.market.service.IGoodsService;
import com.xxx.market.vo.FavorVo;
import com.xxx.market.vo.RespBean;
import com.xxx.market.vo.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
@Controller
@RequestMapping("/favor")
public class FavorController {

    @Autowired
    IGoodsService goodsService;

    @Autowired
    IFavorService favorService;

    @ResponseBody
    @PostMapping("/add/{goodsId}")
    public RespBean add(@PathVariable("goodsId") Long goodsId, HttpSession session){
        Long userId = ((User)session.getAttribute("user")).getId();
        Favor one = favorService.getOne(new QueryWrapper<Favor>().eq("user_id", userId).eq("goods_id", goodsId));
        // 第一次加入收藏
        if (one == null) {
            Favor favor = new Favor();
            favor.setUserId(userId);
            favor.setGoodsId(goodsId);
            favor.setCreateTime(new Date());
            favorService.save(favor);
        }else {
            // 已经存在于我的收藏中
            if (one.getIsCancel() == 0) {
                return RespBean.error(RespBeanEnum.REPEAT_FAVOR);
            }else {
                // 曾经收藏过但被取消了
                one.setIsCancel(0);
                favorService.updateById(one);
            }
        }

        return RespBean.success();
    }

    @RequestMapping("/list")
    public String toFavorList(HttpSession session) {
        Long userId = ((User)session.getAttribute("user")).getId();
        List<FavorVo> favorVoList =  favorService.getFavorVoList(userId);
        session.setAttribute("favorVoList", favorVoList);

        return "favorList";
    }

    @GetMapping("/cancel/{goodsId}")
    public String cancel(@PathVariable("goodsId") Long goodsId, HttpSession session) {
        Long userId = ((User)session.getAttribute("user")).getId();
        Favor one = favorService.getOne(new QueryWrapper<Favor>().eq("goods_id", goodsId).eq("user_id", userId));
        one.setIsCancel(1);
        favorService.updateById(one);

        return "forward:/favor/list";
    }

}
