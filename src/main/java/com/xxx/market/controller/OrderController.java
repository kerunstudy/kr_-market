package com.xxx.market.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xxx.market.domain.Address;
import com.xxx.market.domain.Order;
import com.xxx.market.domain.OrderItem;
import com.xxx.market.domain.User;
import com.xxx.market.service.IAddressService;
import com.xxx.market.service.IOrderItemService;
import com.xxx.market.service.IOrderService;
import com.xxx.market.vo.OrderItemVo;
import com.xxx.market.vo.OrderVo;
import com.xxx.market.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kerun
 * @since 2022-05-01
 */
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    IOrderItemService orderItemService;

    @Autowired
    IOrderService orderService;

    @Autowired
    IAddressService addressService;

    @GetMapping("/list")
    public String toOrderList(HttpSession session) {
        Long userId = ((User)session.getAttribute("user")).getId();
        List<OrderVo> orderVoList = orderService.getOrderVoList(userId);
        for (OrderVo orderVo : orderVoList) {
            Address address = addressService.getOne(new QueryWrapper<Address>().eq("id", orderVo.getDeliveryAddrId()));
            orderVo.setAddress(address);
            BigDecimal total = new BigDecimal(0);
            List<OrderItemVo> orderItemVoList =  orderItemService.getOrderItemVoList(orderVo.getId());
            for (OrderItemVo orderItemVo : orderItemVoList) {
                BigDecimal itemTotal = orderItemVo.getGoodsPrice().multiply(new BigDecimal(orderItemVo.getGoodsCount()));
                total = total.add(itemTotal);
            }
            orderVo.setTotalPrice(total);
            orderVo.setOrderItemVos(orderItemVoList);
        }
        session.setAttribute("orderVoList", orderVoList);

        return "orderList";
    }

    @DeleteMapping("/cancel/{orderId}")
    @ResponseBody
    public RespBean cancel(@PathVariable("orderId") Long orderId) throws InterruptedException {
        Map<String,Object> map = new HashMap<>();
        map.put("order_id", orderId);
        // 先根据orderId删除orderItem
        orderItemService.remove(new QueryWrapper<OrderItem>().eq("order_id", orderId));
        // 再删除Order
        orderService.remove(new QueryWrapper<Order>().eq("id", orderId));
        // 等待0.3秒让数据库进行反应
        Thread.sleep(300);

        return RespBean.success();
    }
}
