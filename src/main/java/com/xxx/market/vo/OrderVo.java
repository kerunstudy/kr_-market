package com.xxx.market.vo;

import com.xxx.market.domain.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Kerun
 * @date 2022/5/3 - 21:25
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderVo {
    /**
     * 订单ID
     */
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 收获地址ID
     */
    private Long deliveryAddrId;

    /**
     * 收获地址详情
     */
    private Address address;

    /**
     * 1pc,2android,3ios
     */
    private Integer orderChannel;

    /**
     * 订单状态,0新建未支付,1已支付,2已发货,3已收货,4已退款,5已完成
     */
    private Integer status;

    /**
     * 订单的创建时间
     */
    private Date createTime;

    /**
     * 支付时间
     */
    private Date payDate;

    /**
     * 订单是否取消
     */
    private Integer isCancel;

    /**
     * 订单包含的订单项
     */
    private List<OrderItemVo> orderItemVos;

    /**
     * 订单总计
     */
    private BigDecimal totalPrice;
}
