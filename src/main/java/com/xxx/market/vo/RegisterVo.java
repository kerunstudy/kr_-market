package com.xxx.market.vo;

import com.xxx.market.validator.annotation.IsMobile;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author Kerun
 * @date 2022/5/6 - 17:35
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterVo {

    /**
     * 手机号
     */
    @NotNull
    @IsMobile
    private String mobile;

    /**
     * 第一次输入的密码
     */
    @NotNull
    private String firstPass;

    /**
     * 确认密码
     */
    @NotNull
    private String confirmPass;
}
