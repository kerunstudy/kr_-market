package com.xxx.market.vo;

import com.xxx.market.validator.annotation.IsMobile;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Kerun
 * @date 2022/5/9 - 18:30
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditVo {

    /**
     * 用户ID,手机号码
     */
    @NotNull
    @IsMobile
    private String phone;

    /**
     * 昵称
     */
    @NotNull
    private String nickname;

    /**
     * M:男;F:女
     */
    @NotNull
    private String gender;

}
