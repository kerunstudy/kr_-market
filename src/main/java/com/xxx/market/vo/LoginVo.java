package com.xxx.market.vo;

import com.xxx.market.validator.annotation.IsMobile;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Kerun
 * @date 2022/4/29 - 18:29
 */

@Data
public class LoginVo {
    @NotNull
    @IsMobile
    private String mobile;

    @NotNull
    private String password;
}