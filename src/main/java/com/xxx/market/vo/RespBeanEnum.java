package com.xxx.market.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Kerun
 * @date 2021/12/21 - 22:19
 */

@Getter
@ToString
@AllArgsConstructor
public enum RespBeanEnum {
    // 通用
    SUCCESS(200,"SUCCESS"),
    ERROR(500,"服务端异常"),
    // 注册模块
    MOBILE_EXIST(500101, "手机号已存在"),
    PASSWORD_DIFFERENT(500102, "两次输入的密码不相同"),
    // 登录模块
    LOGIN_EMPTY_ERROR(500209, "手机号或密码为空"),
    LOGIN_ERROR(500210, "手机号或密码错误"),
    MOBILE_ERROR(500211,"手机号码格式不正确"),
    BIND_ERROR(500212,"参数校验异常"),
    // 秒杀模块 5005xx
    EMPTY_STOCK(500500, "库存不足"),
    REPEAT_ERROR(500501, "该商品每人限购一件！"),
    // 收藏模块
    REPEAT_FAVOR(500601,"已收藏过该商品"),
    // 修改密码模块
    PASSWORD_ERROR(500701,"原密码输入错误")
    ;

    private final Integer code;
    private final String message;

//    private RespBeanEnum() {
//    }

//    RespBeanEnum(Integer code, String message) {
//        this.code = code;
//        this.message = message;
//    }
//
//    public Integer getCode() {
//        return code;
//    }
//
//    public String getMessage() {
//        return message;
//    }


}
