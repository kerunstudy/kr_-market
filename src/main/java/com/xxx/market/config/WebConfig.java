package com.xxx.market.config;

import com.xxx.market.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.List;

/**
 * @author Kerun
 * @date 2022/5/1 - 14:03
 */

@Configuration
public class WebConfig implements WebMvcConfigurer {
    // 1.   src/main/java
    //      src/main/resources/static/
    // /
    // 2. src/main/java/com/xxx/market/config/web  /com/xxx/market/config/web
    private static final List<String> INCLUDE_PATH = Arrays.asList("/cart/**","/user/info","/favor/**","/order/**","/address/**");
    private static final List<String> EXCLUDE_PATH = Arrays.asList("/assets/**");

    @Autowired
    LoginInterceptor loginInterceptor;


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns(INCLUDE_PATH)
                .excludePathPatterns(EXCLUDE_PATH);
    }
}
