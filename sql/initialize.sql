create database if not exists kr;
use kr;

-- 用户表
create table if not exists kr_user(
	`id` BIGINT(20) NOT NULL COMMENT '用户ID,手机号码',
    `nickname` VARCHAR(255) NOT NULL COMMENT '昵称',
    `password` VARCHAR(32) DEFAULT NULL COMMENT 'MD5+salt',
    `salt` VARCHAR(10) DEFAULT NULL,
    `avatars` VARCHAR(128) DEFAULT NULL COMMENT '头像图片所在位置',
    `gender` char(1) comment 'M:男;F:女',
    `user_level` tinyint(4) default '0' comment '会员级别, 0普通会员,1VIP,2超级VIP',
    `register_time` datetime DEFAULT NULL COMMENT '注册时间',
    `last_login_time` datetime DEFAULT NULL COMMENT '最后一次登录时间',
    primary key(id)
)engine = InnoDB default charset = utf8mb4;

-- 商品表
create table if not exists kr_goods (
	`id` bigint(20) not null auto_increment comment '商品ID',
    `goods_name` varchar(128) default null comment '商品名称',
    `goods_title` varchar(128) default null comment '商品标题',
    `goods_img` varchar(128) default null comment '商品图片',
    `goods_detail` longtext comment '商品详情',
    `goods_price` decimal(10,2) default '0.00' comment '商品价格',
    `goods_stock` int(11) default '0' comment '商品库存，-1表示没有限制',
	primary key(`id`)
)engine = InnoDB auto_increment = 1 default charset = utf8mb4;

-- 订单表
create table if not exists kr_order (
	`id` bigint(20) not null auto_increment comment '订单ID',
    `user_id` bigint(20) default null comment '用户ID',
    `delivery_addr_id` bigint(20) default null comment '收获地址ID',
    `order_channel` tinyint(4) default '0' comment '1pc,2android,3ios',
    `status` tinyint(4) default '0' comment '订单状态,0新建未支付,1已支付,2已发货,3已收货,4已退款,5已完成',
    `create_time` datetime default null comment '订单的创建时间',
    `pay_date` datetime default null comment '支付时间',
    `is_cancel` tinyint(4) default '0' comment '订单是否取消',
    primary key(`id`)
)engine = InnoDB auto_increment = 1 default charset = utf8mb4;

-- 订单项表
create table if not exists kr_order_item (
	`id` bigint(20) not null auto_increment comment '订单项ID',
    `order_id` bigint(20) not null comment '订单ID',
    -- 冗余过来的商品数据
    `goods_id` bigint(20) default null comment '商品ID',
    `goods_count` int(11) default 0 comment '购买数量',
    primary key(`id`)
)engine = InnoDB auto_increment = 1 default charset = utf8mb4;

-- 购物车项表
create table if not exists kr_cart(
	`id` bigint(20) not null auto_increment comment '主键ID',
    `user_id` bigint(20) not null comment '用户ID',
    `goods_id` bigint(20) not null comment '商品ID',
	`goods_count` int(11) default '0' comment '商品总数',
    `create_time` datetime default null comment '创建时间',
    `is_delete` tinyint(4) default '0' comment '是否删除',
    primary key(`id`)
)engine = InnoDB auto_increment = 1 default charset = utf8mb4;

-- 我的收藏表
create table if not exists kr_favor(
	`id` bigint(20) not null auto_increment comment '主键ID',
    `user_id` bigint(20) not null comment '用户ID',
    `goods_id` bigint(20) not null comment '商品ID',
    `create_time` datetime default null comment '收藏时间',
    `is_cancel` tinyint(4) not null default '0' comment '是否取消收藏标志,0未取消,1取消',
    primary key(`id`)
)engine = InnoDB auto_increment = 1 default charset = utf8mb4;

-- 收货地址表
create table if not exists kr_address(
	`id` bigint(20) not null auto_increment comment '主键ID',
    `user_id` bigint(20) not null comment '用户ID',
    `consignee` varchar(64) not null comment '收件人',
    `phone_num` varchar(11) comment '联系方式',
    `province` varchar(10) comment '省份',
    `city` varchar(10) comment '市',
    `district` varchar(10) comment '区',
    `address_detail` varchar(128) comment '详细地址',
    `is_default` int(1) not null default '0' comment '是否默认地址',
    primary key(`id`)
)engine = InnoDB auto_increment = 1 default charset = utf8mb4;

insert into `kr_user` values('13510543405','Noth丶','b7797cce01b4b131b433b6acf4add449','1a2b3c4d','','M','2',now(),now());

INSERT INTO `kr`.`kr_address`(`user_id`,`consignee`,`phone_num`,`province`,`city`,`district`,`address_detail`,`is_default`) VALUES
('13510543405','ckr','13510543405','广东','深圳','龙岗','五联社区承翰陶源','1');

INSERT INTO `kr`.`kr_goods`(`goods_name`,`goods_title`,`goods_img`,`goods_detail`,`goods_price`,`goods_stock`) VALUES
('Classic Spring','Classic Spring','/assets/images/men-01.jpg','','120.00','30'),
('Air Force 1 X','Air Force 1 X','/assets/images/men-02.jpg','','90.00','30'),
('Love Nana ‘20','Love Nana ‘20','/assets/images/men-03.jpg','','150.00','30'),
('New Green Jacket','New Green Jacket','/assets/images/women-01.jpg','','75.00','30'),
('Classic Dress','Classic Dress','/assets/images/women-02.jpg','','45.00','30'),
('Spring Collection','Spring Collection','/assets/images/women-03.jpg','','130.00','30'),
('School Collection','School Collection','/assets/images/kid-01.jpg','','80.00','30'),
('Summer Cap','Summer Cap','/assets/images/kid-02.jpg','','12.00','30'),
('Classic Kid','Classic Kid','/assets/images/kid-03.jpg','','30.00','30');



